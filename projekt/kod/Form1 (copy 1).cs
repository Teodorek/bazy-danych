﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace ProgramBiblioteka
{
    public partial class Form1 : Form
    {
        static bool autenthicated = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void logIn_Click(object sender, EventArgs e)
        {
            LogIn();
        }

        public void LogIn()
        {
            autenthicated = false;

            SqlConnection sqlConnection1 = new SqlConnection("Data Source=Teodor;Initial Catalog=baza;Integrated Security=True");
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SELECT 1 FROM Pracownik where login = '" + loginBox.Text + "'";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();
            reader = cmd.ExecuteReader();
            if (reader.HasRows == false)
            {
                reader.Close();
                resultLabel.Text = "Nie ma takiego użytkownika";
            }
            else
            {
                reader.Close();
                cmd.CommandText = "SELECT hasło FROM Pracownik where login = '" + loginBox.Text + "'";

                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    string hashFromDatabase = reader[0].ToString();
                    string hash = CalculateMD5Hash(passwordBox.Text);

                    if (hash.Equals(hashFromDatabase))
                        autenthicated = true;
                    else
                        resultLabel.Text = "Nieprawidłowe hasło!";
                }
                sqlConnection1.Close();
            }

            if (autenthicated)
            {
                this.Hide();
                 Form2 form2 = new Form2();
                form2.Show();
            }

        }

        public string CalculateMD5Hash(string input)

        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            { 
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void passwordBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                LogIn();
        }
    }
}
