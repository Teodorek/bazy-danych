﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramBiblioteka
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }

        private void Form3_Enter(object sender, EventArgs e)
        {
            findAllBooks(); 
        }

        private void Form3_Shown(object sender, EventArgs e)
        {
            findAllBooks();
        }

        void findAllBooks()
        {
            sql query = new sql();

            string _query = "SELECT * FROM ksiazki_view;";
            List<object> result = query.getResultOftQuery(_query, 0);   //sygnatura
            List<object> result2 = query.getResultOftQuery(_query, 3);   //tytul ksiazki
            List<object> result3 = query.getResultOftQuery(_query, 5);   //imie autora
            List<object> result4 = query.getResultOftQuery(_query, 6);   //nazwisko autora

            listBox1.Items.Clear();

            while (result != null && result.Count > 0)
            {
                string tmp = result.First().ToString() + " - " + result2.First().ToString()
                    + " - " + result3.First().ToString() + " " + result4.First().ToString();
                result.RemoveAt(0);
                result2.RemoveAt(0);
                result3.RemoveAt(0);
                result4.RemoveAt(0);
                listBox1.Items.Add(tmp);
            }
        }

        private void titleBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13 && titleOfBookBox.Text == "")
            {
                findAllBooks();
            }
            else
            if(e.KeyChar == 13)
            {
                sql query = new sql();

                string _query = "SELECT * FROM ksiazki_view WHERE tytul = '" + titleOfBookBox.Text + "'";
                List<object> result = query.getResultOftQuery(_query, 0);   //sygnatura
                List<object> result2= query.getResultOftQuery(_query, 3);   //tytul ksiazki
                List<object> result3 = query.getResultOftQuery(_query, 5);   //imie autora
                List<object> result4 = query.getResultOftQuery(_query, 6);   //nazwisko autora

                listBox1.Items.Clear();

                while(result != null && result.Count > 0)
                {
                    string tmp = result.First().ToString() + " - " + result2.First().ToString()
                        + " - " + result3.First().ToString() + " " + result4.First().ToString();
                    result.RemoveAt(0);
                    result2.RemoveAt(0);
                    result3.RemoveAt(0);
                    result4.RemoveAt(0);
                    listBox1.Items.Add(tmp);
                }

            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string rowValue = listBox1.SelectedItem.ToString();
            string[] elements = rowValue.Split(' ');        //pierwszy element - sygnatura
            findBook(elements[0]);
        }

        void findBook(string sygnatura)
        {
            sql query = new sql();
            string _query = "SELECT * FROM ksiazki_view where sygnatura = " + sygnatura + ";";
            List<List<object>> result = new List<List<object>>();

            for (int i = 0; i < 7; i++)
                result.Add(query.getResultOftQuery(_query, i));

            sygnaturaBox.Text = result.ElementAt(0).ElementAt(0).ToString() + "";
            isbnBox.Text = result.ElementAt(1).ElementAt(0).ToString() + "";
            wydawnictwoBox.Text = result.ElementAt(2).ElementAt(0).ToString() + "";
            tytulBox.Text = result.ElementAt(3).ElementAt(0).ToString() + "";
            rokBox.Text = result.ElementAt(4).ElementAt(0).ToString() + "";
            autorBox.Text = result.ElementAt(5).ElementAt(0).ToString() + " "+result.ElementAt(6).ElementAt(0).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tytulBox.Clear();
            autorBox.Clear();
            rokBox.Clear();
            sygnaturaBox.Clear();
            isbnBox.Clear();
            wydawnictwoBox.Clear();
        }

        private void addBook_Click(object sender, EventArgs e)
        {
            sql obj = new sql();
            if (sygnaturaBox.Text == "" || autorBox.Text == "" || rokBox.Text == "" || wydawnictwoBox.Text == "" || tytulBox.Text == "" || isbnBox.Text == "")
            {
                MessageBox.Show("Nie wypełniono wszystkich wymaganych pól");
            }
            else
            {
                string[] autors = autorBox.Text.Split(',');
                string[] AutorID = new string[autors.Length];

                int i = 0;
                foreach (string autor in autors)
                    AutorID[i++] = obj.addAutor(autor);

                string rokID = obj.addYear(rokBox.Text);
                string wydawnictwoID = obj.addPrint(wydawnictwoBox.Text);

                int check = obj.addBook(sygnaturaBox.Text, wydawnictwoID, rokID, tytulBox.Text, isbnBox.Text);
                if (check == 0)
                    obj.addGroupOfAutors(AutorID, sygnaturaBox.Text);
                else
                    MessageBox.Show("Podana sygnatura istnieje w bazie");

                findAllBooks();
            }
        }
    }
}
