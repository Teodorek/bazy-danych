﻿public string addAutor(string nameAndSurname)      //metoda zwraca autorID
{
    string[] tmp = nameAndSurname.Split(' ');
    string checkingQuery = "SELECT (1) FROM Autor where imie = '" + tmp[0] + "' AND nazwisko = '" + tmp[1] + "';";
    int check = getResultOftQuery(checkingQuery, 0).Count();

    if(check == 0)
    {
	string query = "INSERT INTO Autor (imie, nazwisko) VALUES ('" + tmp[0] + "','" + tmp[1] + "');";
	appendQuery(query);
    }

    string queryAutorID =
    	   "SELECT AutorID FROM Autor where imie = '" + tmp[0] + "' AND nazwisko = '" + tmp[1] + "';";
    string AutorID = getResultOftQuery(queryAutorID, 0).First().ToString();

    return AutorID;
}

public void addGroupOfAutors(string [] AutorId, string sygnatura)
{
    foreach(string id in AutorId)
    {
	string query = "INSERT INTO Grupa_autor(autorID, sygnatura) VALUES (" + id + ",'" + sygnatura + "');";
	appendQuery(query);
    }
}

public int addBook(string sygnatura, string wydawnictwoID, string rokWydaniaID, string tytul, string ISBN)
{
    string _query = "SELECT (0) FROM Ksiazki where sygnatura = '" + sygnatura + "';";
    int check = getResultOftQuery(_query, 0).Count();
    if (check == 0)
    {
	string query = "INSERT INTO Ksiazki(sygnatura, wydawnictwoId, rok_wydaniaID, tytul, ISBN) 
	VALUES (" + sygnatura + "," + wydawnictwoID + "," + rokWydaniaID + ",'" + tytul + "'," + ISBN + ");";
	appendQuery(query);
    }
    return check;
}

public string addYear(string year)
{
    string query = "SELECT (1) FROM Rok_wydania where rok = " + year + ";";
    int check = getResultOftQuery(query, 0).Count();

    if(check == 0)
    {
	string query2 = "INSERT INTO Rok_wydania(rok) VALUES (" + year + ");";
	appendQuery(query2);
    }

    string query3 = "SELECT rok_wydaniaID FROM Rok_wydania where rok = " + year + ";";
    return getResultOftQuery(query3, 0).First().ToString();
}

public string addPrint(string print)
{
    string query = "SELECT (1) FROM Wydawnictwo where wydawnictwo = '" + print + "';";
    int check = getResultOftQuery(query, 0).Count();

    if(check == 0)
    {
	string query2 = "INSERT INTO Wydawnictwo (wydawnictwo) VALUES ('" + print + "');";
	appendQuery(query2);
    }

    string query3 = "SELECT wydawnictwoID FROM Wydawnictwo where wydawnictwo = '" + print + "';";
    return getResultOftQuery(query3, 0).First().ToString();
}
public string addAddress(string location, string street, string house, string home, string number)
{
    string query = "SELECT (1) FROM Adres where miejscowość = '" + location +  "' AND ulica = '"  + street + "' 
    AND [nr domu] = '" + house + "' AND [nr mieszkania] = '" + home + "' AND [nr telefonu] = '" + number + "';";
    int check = getResultOftQuery(query, 0).Count();
    if(check == 0)
    {
	string query2 = "INSERT INTO Adres(miejscowość, ulica, [nr domu], [nr mieszkania], [nr telefonu]) 
VALUES ('" + location + "','" + street + "','" + house + "','" + home + "','" + number + "');";
	appendQuery(query2);
    }
    string query3 = "SELECT AdresID FROM Adres where miejscowość = '" + location + "' 
    AND ulica = '" + street + "' AND [nr domu] = '" + house + "' 
    AND [nr mieszkania] = '" + home + "' AND [nr telefonu] = '" + number + "';";
    string AddressID = getResultOftQuery(query3, 0).First().ToString();
    return AddressID;
}

public int addReader(string pesel, string forename, string surname, string  addressID, string regonID)
{
    string query = "SELECT (1) FROM Czytelnik where pesel = '" + pesel + "';";
    int check = getResultOftQuery(query, 0).Count();
    if(check == 0)
    {
	string query2 = "INSERT INTO Czytelnik(pesel, AdresID, imie, nazwisko, regonID) 
	VALUES ('" + pesel + "'," + addressID + ",'" + forename + "','" + surname + "','" + regonID +
	    "')" ;
	appendQuery(query2);
    }
    return check;
}