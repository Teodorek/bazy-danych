﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramBiblioteka
{
    public partial class Form2 : Form
    {
        private sql sqlQuery = new sql();

        public Form2()
        {
            InitializeComponent();
            string query = "SELECT COUNT(*) FROM Czytelnik;";
            numberOfUsersLabel.Text = "Liczba czytelników:" + sqlQuery.getResultOftQuery(query,0).First().ToString();
            query = "SELECT COUNT(*) FROM Ksiazki;";
            numberOfBooksLabel.Text = "Liczba książek: " + sqlQuery.getResultOftQuery(query,0).First().ToString();
            query = "SELECT COUNT(*) FROM Szczegoly_wypozyczenia;";
            numberOfBorrowedBooksLabel.Text = "Liczba wypożyczonych książek: " + sqlQuery.getResultOftQuery(query, 0).First().ToString();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 form3 = new Form3();
            form3.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 form4 = new Form4();
            form4.Show();
        }
    }
}
