﻿namespace ProgramBiblioteka
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.titleOfBookBox = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.sygnaturaBox = new System.Windows.Forms.TextBox();
            this.wydawnictwoLabel = new System.Windows.Forms.Label();
            this.isbnLabel = new System.Windows.Forms.Label();
            this.rokLabel = new System.Windows.Forms.Label();
            this.autorLabel = new System.Windows.Forms.Label();
            this.tytulLabel = new System.Windows.Forms.Label();
            this.wydawnictwoBox = new System.Windows.Forms.TextBox();
            this.isbnBox = new System.Windows.Forms.TextBox();
            this.rokBox = new System.Windows.Forms.TextBox();
            this.autorBox = new System.Windows.Forms.TextBox();
            this.tytulBox = new System.Windows.Forms.TextBox();
            this.addBook = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.sygnatura2Box = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.wydawnictwo2Box = new System.Windows.Forms.TextBox();
            this.isbn2Box = new System.Windows.Forms.TextBox();
            this.rok2Box = new System.Windows.Forms.TextBox();
            this.autor2Box = new System.Windows.Forms.TextBox();
            this.tytul2Box = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tytuł: ";
            // 
            // titleOfBookBox
            // 
            this.titleOfBookBox.Location = new System.Drawing.Point(56, 22);
            this.titleOfBookBox.Name = "titleOfBookBox";
            this.titleOfBookBox.Size = new System.Drawing.Size(100, 20);
            this.titleOfBookBox.TabIndex = 1;
            this.titleOfBookBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.titleBox_KeyPress);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(15, 99);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(299, 225);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Lista książek:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.sygnaturaBox);
            this.groupBox1.Controls.Add(this.wydawnictwoLabel);
            this.groupBox1.Controls.Add(this.isbnLabel);
            this.groupBox1.Controls.Add(this.rokLabel);
            this.groupBox1.Controls.Add(this.autorLabel);
            this.groupBox1.Controls.Add(this.tytulLabel);
            this.groupBox1.Controls.Add(this.wydawnictwoBox);
            this.groupBox1.Controls.Add(this.isbnBox);
            this.groupBox1.Controls.Add(this.rokBox);
            this.groupBox1.Controls.Add(this.autorBox);
            this.groupBox1.Controls.Add(this.tytulBox);
            this.groupBox1.Location = new System.Drawing.Point(346, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 121);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dane książki";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Sygnatura:";
            // 
            // sygnaturaBox
            // 
            this.sygnaturaBox.Location = new System.Drawing.Point(293, 95);
            this.sygnaturaBox.MaxLength = 9;
            this.sygnaturaBox.Name = "sygnaturaBox";
            this.sygnaturaBox.ReadOnly = true;
            this.sygnaturaBox.Size = new System.Drawing.Size(100, 20);
            this.sygnaturaBox.TabIndex = 10;
            // 
            // wydawnictwoLabel
            // 
            this.wydawnictwoLabel.AutoSize = true;
            this.wydawnictwoLabel.Location = new System.Drawing.Point(210, 61);
            this.wydawnictwoLabel.Name = "wydawnictwoLabel";
            this.wydawnictwoLabel.Size = new System.Drawing.Size(77, 13);
            this.wydawnictwoLabel.TabIndex = 9;
            this.wydawnictwoLabel.Text = "Wydawnictwo:";
            // 
            // isbnLabel
            // 
            this.isbnLabel.AutoSize = true;
            this.isbnLabel.Location = new System.Drawing.Point(210, 26);
            this.isbnLabel.Name = "isbnLabel";
            this.isbnLabel.Size = new System.Drawing.Size(35, 13);
            this.isbnLabel.TabIndex = 8;
            this.isbnLabel.Text = "ISBN:";
            // 
            // rokLabel
            // 
            this.rokLabel.AutoSize = true;
            this.rokLabel.Location = new System.Drawing.Point(6, 98);
            this.rokLabel.Name = "rokLabel";
            this.rokLabel.Size = new System.Drawing.Size(72, 13);
            this.rokLabel.TabIndex = 7;
            this.rokLabel.Text = "Rok wydania:";
            // 
            // autorLabel
            // 
            this.autorLabel.AutoSize = true;
            this.autorLabel.Location = new System.Drawing.Point(6, 61);
            this.autorLabel.Name = "autorLabel";
            this.autorLabel.Size = new System.Drawing.Size(35, 13);
            this.autorLabel.TabIndex = 6;
            this.autorLabel.Text = "Autor:";
            // 
            // tytulLabel
            // 
            this.tytulLabel.AutoSize = true;
            this.tytulLabel.Location = new System.Drawing.Point(6, 22);
            this.tytulLabel.Name = "tytulLabel";
            this.tytulLabel.Size = new System.Drawing.Size(35, 13);
            this.tytulLabel.TabIndex = 5;
            this.tytulLabel.Text = "Tytuł:";
            // 
            // wydawnictwoBox
            // 
            this.wydawnictwoBox.Location = new System.Drawing.Point(293, 54);
            this.wydawnictwoBox.Name = "wydawnictwoBox";
            this.wydawnictwoBox.ReadOnly = true;
            this.wydawnictwoBox.Size = new System.Drawing.Size(100, 20);
            this.wydawnictwoBox.TabIndex = 4;
            // 
            // isbnBox
            // 
            this.isbnBox.Location = new System.Drawing.Point(293, 19);
            this.isbnBox.MaxLength = 11;
            this.isbnBox.Name = "isbnBox";
            this.isbnBox.ReadOnly = true;
            this.isbnBox.Size = new System.Drawing.Size(100, 20);
            this.isbnBox.TabIndex = 3;
            // 
            // rokBox
            // 
            this.rokBox.Location = new System.Drawing.Point(93, 95);
            this.rokBox.Name = "rokBox";
            this.rokBox.ReadOnly = true;
            this.rokBox.Size = new System.Drawing.Size(100, 20);
            this.rokBox.TabIndex = 2;
            // 
            // autorBox
            // 
            this.autorBox.Location = new System.Drawing.Point(93, 54);
            this.autorBox.Name = "autorBox";
            this.autorBox.ReadOnly = true;
            this.autorBox.Size = new System.Drawing.Size(100, 20);
            this.autorBox.TabIndex = 1;
            // 
            // tytulBox
            // 
            this.tytulBox.Location = new System.Drawing.Point(93, 19);
            this.tytulBox.Name = "tytulBox";
            this.tytulBox.ReadOnly = true;
            this.tytulBox.Size = new System.Drawing.Size(100, 20);
            this.tytulBox.TabIndex = 0;
            // 
            // addBook
            // 
            this.addBook.Location = new System.Drawing.Point(355, 298);
            this.addBook.Name = "addBook";
            this.addBook.Size = new System.Drawing.Size(118, 26);
            this.addBook.TabIndex = 5;
            this.addBook.Text = "Dodaj książkę";
            this.addBook.UseVisualStyleBackColor = true;
            this.addBook.Click += new System.EventHandler(this.addBook_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(621, 298);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 26);
            this.button1.TabIndex = 6;
            this.button1.Text = "Wyczyść";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.sygnatura2Box);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.wydawnictwo2Box);
            this.groupBox2.Controls.Add(this.isbn2Box);
            this.groupBox2.Controls.Add(this.rok2Box);
            this.groupBox2.Controls.Add(this.autor2Box);
            this.groupBox2.Controls.Add(this.tytul2Box);
            this.groupBox2.Location = new System.Drawing.Point(346, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(454, 121);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dane książki do wypełnienia";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Sygnatura:";
            // 
            // sygnatura2Box
            // 
            this.sygnatura2Box.Location = new System.Drawing.Point(293, 95);
            this.sygnatura2Box.MaxLength = 9;
            this.sygnatura2Box.Name = "sygnatura2Box";
            this.sygnatura2Box.Size = new System.Drawing.Size(100, 20);
            this.sygnatura2Box.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(210, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Wydawnictwo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(210, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "ISBN:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Rok wydania:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Autor:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Tytuł:";
            // 
            // wydawnictwo2Box
            // 
            this.wydawnictwo2Box.Location = new System.Drawing.Point(293, 54);
            this.wydawnictwo2Box.Name = "wydawnictwo2Box";
            this.wydawnictwo2Box.Size = new System.Drawing.Size(100, 20);
            this.wydawnictwo2Box.TabIndex = 4;
            // 
            // isbn2Box
            // 
            this.isbn2Box.Location = new System.Drawing.Point(293, 19);
            this.isbn2Box.MaxLength = 11;
            this.isbn2Box.Name = "isbn2Box";
            this.isbn2Box.Size = new System.Drawing.Size(100, 20);
            this.isbn2Box.TabIndex = 3;
            // 
            // rok2Box
            // 
            this.rok2Box.Location = new System.Drawing.Point(93, 95);
            this.rok2Box.Name = "rok2Box";
            this.rok2Box.Size = new System.Drawing.Size(100, 20);
            this.rok2Box.TabIndex = 2;
            // 
            // autor2Box
            // 
            this.autor2Box.Location = new System.Drawing.Point(93, 54);
            this.autor2Box.Name = "autor2Box";
            this.autor2Box.Size = new System.Drawing.Size(100, 20);
            this.autor2Box.TabIndex = 1;
            // 
            // tytul2Box
            // 
            this.tytul2Box.Location = new System.Drawing.Point(93, 19);
            this.tytul2Box.Name = "tytul2Box";
            this.tytul2Box.Size = new System.Drawing.Size(100, 20);
            this.tytul2Box.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(196, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 26);
            this.button2.TabIndex = 13;
            this.button2.Text = "Szukaj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 348);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.addBook);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.titleOfBookBox);
            this.Controls.Add(this.label1);
            this.Name = "Form3";
            this.Text = "Obsługa książek";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form3_FormClosed);
            this.Shown += new System.EventHandler(this.Form3_Shown);
            this.Enter += new System.EventHandler(this.Form3_Enter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox titleOfBookBox;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label wydawnictwoLabel;
        private System.Windows.Forms.Label isbnLabel;
        private System.Windows.Forms.Label rokLabel;
        private System.Windows.Forms.Label autorLabel;
        private System.Windows.Forms.Label tytulLabel;
        private System.Windows.Forms.TextBox wydawnictwoBox;
        private System.Windows.Forms.TextBox isbnBox;
        private System.Windows.Forms.TextBox rokBox;
        private System.Windows.Forms.TextBox autorBox;
        private System.Windows.Forms.TextBox tytulBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox sygnaturaBox;
        private System.Windows.Forms.Button addBook;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox sygnatura2Box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox wydawnictwo2Box;
        private System.Windows.Forms.TextBox isbn2Box;
        private System.Windows.Forms.TextBox rok2Box;
        private System.Windows.Forms.TextBox autor2Box;
        private System.Windows.Forms.TextBox tytul2Box;
        private System.Windows.Forms.Button button2;
    }
}