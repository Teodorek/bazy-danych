﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramBiblioteka
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            findAllreaders();
        }
        void findAllreaders()
        {
            sql query = new sql();

            string _query = "SELECT * FROM Czytelnik;";
            List<object> result = query.getResultOftQuery(_query, 0);   // pesel
            List<object> result2 = query.getResultOftQuery(_query, 2);   //imie
            List<object> result3 = query.getResultOftQuery(_query, 3);  // nazwisko
            while (result != null && result.Count > 0)
            {
                string tmp = result.First().ToString() + " - " + result2.First().ToString()
                    + " - " + result3.First().ToString();
                result.RemoveAt(0);
                result2.RemoveAt(0);
                result3.RemoveAt(0);
                listOfreaders.Items.Add(tmp);
            }
        }

        private void listOfreaders_SelectedIndexChanged(object sender, EventArgs e)
        {
            var rowValue = listOfreaders.SelectedItem.ToString();
            string[] value = rowValue.Split(' ');
            findReader(value[0]);

        }
        private void findReader(string pesel)
        {
            sql query = new sql();
            string _query = "SELECT * FROM czytelnik_view where pesel = " + pesel + ";";
            List<List<object>> results = new List<List<object>>();
            for (var i = 0; i < 8; ++i)
            {
                results.Add(query.getResultOftQuery(_query, i));
            }

            peselBox.Text = results.ElementAt(0).ElementAt(0).ToString();
            imieBox.Text = results.ElementAt(1).ElementAt(0).ToString();
            nazwiskoBox.Text = results.ElementAt(2).ElementAt(0).ToString();
            miejscowoscBox.Text = results.ElementAt(3).ElementAt(0).ToString();
            ulicaBox.Text = results.ElementAt(4).ElementAt(0).ToString();
            domBox.Text = results.ElementAt(5).ElementAt(0).ToString();
            mieszkanieBox.Text = results.ElementAt(6).ElementAt(0).ToString();
            telefonBox.Text = results.ElementAt(7).ElementAt(0).ToString();
        }
        private void addReader()
        {
            sql query = new sql();
            if (miejscowosc2Box.Text == "" || pesel2Box.Text == "" || imie2Box.Text == "" || nazwisko2Box.Text == "" || dom2Box.Text == "")
            {
                MessageBox.Show("Wypełnij wszystkie wymagane pola");
            }
            else
            {
                string AddresID = query.addAddress(miejscowosc2Box.Text, ulica2Box.Text, dom2Box.Text, mieszkanie2Box.Text, telefon2Box.Text);
                int check = query.addReader(pesel2Box.Text, imie2Box.Text, nazwisko2Box.Text, AddresID, "111111111");
                listOfreaders.Items.Clear();
                findAllreaders();
                if (check!=0) MessageBox.Show("Podany pesel jest już w bazie");
            }
        }

        private void AddreaderButton_Click(object sender, EventArgs e)
        {
            addReader();
        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form2 form1 = new Form2();
            form1.Show();
        }

        private void czyscButton_Click(object sender, EventArgs e)
        {
            peselBox.Clear();
            miejscowoscBox.Clear();
            domBox.Clear();
            mieszkanieBox.Clear();
            telefonBox.Clear();
            imieBox.Clear();
            nazwiskoBox.Clear();
            ulicaBox.Clear();
        }

        private void usunButton_Click(object sender, EventArgs e)
        {
            sql query = new sql();
            string query3 = "DELETE FROM Kara where karaID IN (SELECT szczegolyID FROM Szczegoly_wypozyczenia WHERE peselIDC = '" + peselBox.Text+ "')";
            query.appendQuery(query3);
            string query2 = "DELETE FROM Szczegoly_wypozyczenia WHERE peselIDC = '" + listOfreaders.SelectedItem.ToString().Split(' ')[0]+"';";
            query.appendQuery(query2);
            query2 = "DELETE FROM Czytelnik WHERE pesel = '" + listOfreaders.SelectedItem.ToString().Split(' ')[0] + "';";
            query.appendQuery(query2);
            listOfreaders.Items.Clear();
            findAllreaders();
        }

        private void searchPeselBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 && searchPeselBox.Text == "")
            {
                listOfreaders.Items.Clear();
                findAllreaders();
            }
            else
            if (e.KeyChar == 13)
                findBySurname();
        }

        private void findBySurname()
        {
            sql query = new sql();
            string _query = "SELECT * FROM czytelnik_view where nazwisko = '" + searchPeselBox.Text + "';";
            List<object> result = query.getResultOftQuery(_query, 0);   // pesel
            List<object> result2 = query.getResultOftQuery(_query, 1);   //imie
            List<object> result3 = query.getResultOftQuery(_query, 2);  // nazwisko
            listOfreaders.Items.Clear();
            while (result != null && result.Count > 0)
            {
                string tmp = result.First().ToString() + " - " + result2.First().ToString()
                    + " - " + result3.First().ToString();
                result.RemoveAt(0);
                result2.RemoveAt(0);
                result3.RemoveAt(0);
                listOfreaders.Items.Add(tmp);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (searchPeselBox.Text == "")
                findAllreaders();
            else
                findBySurname();
        }
    }
}
