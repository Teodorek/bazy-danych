﻿namespace ProgramBiblioteka
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.numberOfUsersLabel = new System.Windows.Forms.Label();
            this.numberOfBooksLabel = new System.Windows.Forms.Label();
            this.numberOfBorrowedBooksLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(103, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Menu główne";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(72, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 25);
            this.button1.TabIndex = 1;
            this.button1.Text = "Obsługa czytelnika";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(72, 105);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 25);
            this.button2.TabIndex = 2;
            this.button2.Text = "Obsługa książek";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(72, 148);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(140, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Koniec";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // numberOfUsersLabel
            // 
            this.numberOfUsersLabel.AutoSize = true;
            this.numberOfUsersLabel.Location = new System.Drawing.Point(12, 191);
            this.numberOfUsersLabel.Name = "numberOfUsersLabel";
            this.numberOfUsersLabel.Size = new System.Drawing.Size(111, 13);
            this.numberOfUsersLabel.TabIndex = 4;
            this.numberOfUsersLabel.Text = "Liczba użytkowników:";
            // 
            // numberOfBooksLabel
            // 
            this.numberOfBooksLabel.AutoSize = true;
            this.numberOfBooksLabel.Location = new System.Drawing.Point(12, 215);
            this.numberOfBooksLabel.Name = "numberOfBooksLabel";
            this.numberOfBooksLabel.Size = new System.Drawing.Size(80, 13);
            this.numberOfBooksLabel.TabIndex = 5;
            this.numberOfBooksLabel.Text = "Liczba książek:";
            // 
            // numberOfBorrowedBooksLabel
            // 
            this.numberOfBorrowedBooksLabel.AutoSize = true;
            this.numberOfBorrowedBooksLabel.Location = new System.Drawing.Point(12, 240);
            this.numberOfBorrowedBooksLabel.Name = "numberOfBorrowedBooksLabel";
            this.numberOfBorrowedBooksLabel.Size = new System.Drawing.Size(158, 13);
            this.numberOfBorrowedBooksLabel.TabIndex = 6;
            this.numberOfBorrowedBooksLabel.Text = "Liczba wypożyczonych książek:";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.numberOfBorrowedBooksLabel);
            this.Controls.Add(this.numberOfBooksLabel);
            this.Controls.Add(this.numberOfUsersLabel);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Menu główne";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label numberOfUsersLabel;
        private System.Windows.Forms.Label numberOfBooksLabel;
        private System.Windows.Forms.Label numberOfBorrowedBooksLabel;
    }
}