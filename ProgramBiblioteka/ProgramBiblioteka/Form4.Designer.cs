﻿namespace ProgramBiblioteka
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.searchPeselBox = new System.Windows.Forms.TextBox();
            this.listOfreaders = new System.Windows.Forms.ListBox();
            this.peselBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.imieBox = new System.Windows.Forms.TextBox();
            this.nazwiskoBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.miejscowoscBox = new System.Windows.Forms.TextBox();
            this.ulicaBox = new System.Windows.Forms.TextBox();
            this.mieszkanieBox = new System.Windows.Forms.TextBox();
            this.domBox = new System.Windows.Forms.TextBox();
            this.telefonBox = new System.Windows.Forms.TextBox();
            this.AddreaderButton = new System.Windows.Forms.Button();
            this.czyscButton = new System.Windows.Forms.Button();
            this.usunButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pesel2Box = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.telefon2Box = new System.Windows.Forms.TextBox();
            this.imie2Box = new System.Windows.Forms.TextBox();
            this.dom2Box = new System.Windows.Forms.TextBox();
            this.nazwisko2Box = new System.Windows.Forms.TextBox();
            this.mieszkanie2Box = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ulica2Box = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.miejscowosc2Box = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwisko czytelnika:";
            // 
            // searchPeselBox
            // 
            this.searchPeselBox.Location = new System.Drawing.Point(124, 39);
            this.searchPeselBox.MaxLength = 11;
            this.searchPeselBox.Name = "searchPeselBox";
            this.searchPeselBox.Size = new System.Drawing.Size(100, 20);
            this.searchPeselBox.TabIndex = 1;
            this.searchPeselBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchPeselBox_KeyPress);
            // 
            // listOfreaders
            // 
            this.listOfreaders.FormattingEnabled = true;
            this.listOfreaders.Location = new System.Drawing.Point(15, 87);
            this.listOfreaders.Name = "listOfreaders";
            this.listOfreaders.Size = new System.Drawing.Size(201, 251);
            this.listOfreaders.TabIndex = 2;
            this.listOfreaders.SelectedIndexChanged += new System.EventHandler(this.listOfreaders_SelectedIndexChanged);
            // 
            // peselBox
            // 
            this.peselBox.Location = new System.Drawing.Point(101, 23);
            this.peselBox.MaxLength = 11;
            this.peselBox.Name = "peselBox";
            this.peselBox.ReadOnly = true;
            this.peselBox.Size = new System.Drawing.Size(100, 20);
            this.peselBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Pesel czytelnika:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Imię:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nazwisko:";
            // 
            // imieBox
            // 
            this.imieBox.Location = new System.Drawing.Point(98, 44);
            this.imieBox.MaxLength = 11;
            this.imieBox.Name = "imieBox";
            this.imieBox.ReadOnly = true;
            this.imieBox.Size = new System.Drawing.Size(100, 20);
            this.imieBox.TabIndex = 7;
            // 
            // nazwiskoBox
            // 
            this.nazwiskoBox.Location = new System.Drawing.Point(269, 44);
            this.nazwiskoBox.MaxLength = 11;
            this.nazwiskoBox.Name = "nazwiskoBox";
            this.nazwiskoBox.ReadOnly = true;
            this.nazwiskoBox.Size = new System.Drawing.Size(120, 20);
            this.nazwiskoBox.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Miejscowość:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ulica:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(230, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Nr domu:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(204, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Nr mieszkania:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Nr telefonu:";
            // 
            // miejscowoscBox
            // 
            this.miejscowoscBox.Location = new System.Drawing.Point(98, 70);
            this.miejscowoscBox.MaxLength = 11;
            this.miejscowoscBox.Name = "miejscowoscBox";
            this.miejscowoscBox.ReadOnly = true;
            this.miejscowoscBox.Size = new System.Drawing.Size(100, 20);
            this.miejscowoscBox.TabIndex = 14;
            // 
            // ulicaBox
            // 
            this.ulicaBox.Location = new System.Drawing.Point(98, 102);
            this.ulicaBox.MaxLength = 11;
            this.ulicaBox.Name = "ulicaBox";
            this.ulicaBox.ReadOnly = true;
            this.ulicaBox.Size = new System.Drawing.Size(100, 20);
            this.ulicaBox.TabIndex = 15;
            // 
            // mieszkanieBox
            // 
            this.mieszkanieBox.Location = new System.Drawing.Point(286, 70);
            this.mieszkanieBox.MaxLength = 11;
            this.mieszkanieBox.Name = "mieszkanieBox";
            this.mieszkanieBox.ReadOnly = true;
            this.mieszkanieBox.Size = new System.Drawing.Size(49, 20);
            this.mieszkanieBox.TabIndex = 16;
            // 
            // domBox
            // 
            this.domBox.Location = new System.Drawing.Point(286, 105);
            this.domBox.MaxLength = 11;
            this.domBox.Name = "domBox";
            this.domBox.ReadOnly = true;
            this.domBox.Size = new System.Drawing.Size(49, 20);
            this.domBox.TabIndex = 17;
            // 
            // telefonBox
            // 
            this.telefonBox.Location = new System.Drawing.Point(96, 128);
            this.telefonBox.MaxLength = 11;
            this.telefonBox.Name = "telefonBox";
            this.telefonBox.ReadOnly = true;
            this.telefonBox.Size = new System.Drawing.Size(100, 20);
            this.telefonBox.TabIndex = 18;
            // 
            // AddreaderButton
            // 
            this.AddreaderButton.Location = new System.Drawing.Point(350, 336);
            this.AddreaderButton.Name = "AddreaderButton";
            this.AddreaderButton.Size = new System.Drawing.Size(75, 23);
            this.AddreaderButton.TabIndex = 19;
            this.AddreaderButton.Text = "Dodaj";
            this.AddreaderButton.UseVisualStyleBackColor = true;
            this.AddreaderButton.Click += new System.EventHandler(this.AddreaderButton_Click);
            // 
            // czyscButton
            // 
            this.czyscButton.Location = new System.Drawing.Point(664, 336);
            this.czyscButton.Name = "czyscButton";
            this.czyscButton.Size = new System.Drawing.Size(75, 23);
            this.czyscButton.TabIndex = 20;
            this.czyscButton.Text = "Wyczysc";
            this.czyscButton.UseVisualStyleBackColor = true;
            this.czyscButton.Click += new System.EventHandler(this.czyscButton_Click);
            // 
            // usunButton
            // 
            this.usunButton.Location = new System.Drawing.Point(515, 336);
            this.usunButton.Name = "usunButton";
            this.usunButton.Size = new System.Drawing.Size(75, 23);
            this.usunButton.TabIndex = 21;
            this.usunButton.Text = "Usun";
            this.usunButton.UseVisualStyleBackColor = true;
            this.usunButton.Click += new System.EventHandler(this.usunButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.peselBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.telefonBox);
            this.groupBox1.Controls.Add(this.imieBox);
            this.groupBox1.Controls.Add(this.domBox);
            this.groupBox1.Controls.Add(this.nazwiskoBox);
            this.groupBox1.Controls.Add(this.mieszkanieBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ulicaBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.miejscowoscBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(350, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 150);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Szczegółowe informacje";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Lista czytelników";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.pesel2Box);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.telefon2Box);
            this.groupBox2.Controls.Add(this.imie2Box);
            this.groupBox2.Controls.Add(this.dom2Box);
            this.groupBox2.Controls.Add(this.nazwisko2Box);
            this.groupBox2.Controls.Add(this.mieszkanie2Box);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.ulica2Box);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.miejscowosc2Box);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Location = new System.Drawing.Point(350, 168);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(395, 150);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dane czytelnika do wypełnienia";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Pesel czytelnika:";
            // 
            // pesel2Box
            // 
            this.pesel2Box.Location = new System.Drawing.Point(98, 22);
            this.pesel2Box.MaxLength = 11;
            this.pesel2Box.Name = "pesel2Box";
            this.pesel2Box.Size = new System.Drawing.Size(100, 20);
            this.pesel2Box.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(63, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Imię:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(207, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Nazwisko:";
            // 
            // telefon2Box
            // 
            this.telefon2Box.Location = new System.Drawing.Point(96, 128);
            this.telefon2Box.MaxLength = 9;
            this.telefon2Box.Name = "telefon2Box";
            this.telefon2Box.Size = new System.Drawing.Size(100, 20);
            this.telefon2Box.TabIndex = 18;
            // 
            // imie2Box
            // 
            this.imie2Box.Location = new System.Drawing.Point(98, 44);
            this.imie2Box.MaxLength = 11;
            this.imie2Box.Name = "imie2Box";
            this.imie2Box.Size = new System.Drawing.Size(100, 20);
            this.imie2Box.TabIndex = 7;
            // 
            // dom2Box
            // 
            this.dom2Box.Location = new System.Drawing.Point(286, 105);
            this.dom2Box.MaxLength = 11;
            this.dom2Box.Name = "dom2Box";
            this.dom2Box.Size = new System.Drawing.Size(49, 20);
            this.dom2Box.TabIndex = 17;
            // 
            // nazwisko2Box
            // 
            this.nazwisko2Box.Location = new System.Drawing.Point(269, 44);
            this.nazwisko2Box.MaxLength = 11;
            this.nazwisko2Box.Name = "nazwisko2Box";
            this.nazwisko2Box.Size = new System.Drawing.Size(120, 20);
            this.nazwisko2Box.TabIndex = 8;
            // 
            // mieszkanie2Box
            // 
            this.mieszkanie2Box.Location = new System.Drawing.Point(286, 70);
            this.mieszkanie2Box.MaxLength = 11;
            this.mieszkanie2Box.Name = "mieszkanie2Box";
            this.mieszkanie2Box.Size = new System.Drawing.Size(49, 20);
            this.mieszkanie2Box.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Miejscowość:";
            // 
            // ulica2Box
            // 
            this.ulica2Box.Location = new System.Drawing.Point(98, 102);
            this.ulica2Box.MaxLength = 11;
            this.ulica2Box.Name = "ulica2Box";
            this.ulica2Box.Size = new System.Drawing.Size(100, 20);
            this.ulica2Box.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(49, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "Ulica:";
            // 
            // miejscowosc2Box
            // 
            this.miejscowosc2Box.Location = new System.Drawing.Point(98, 70);
            this.miejscowosc2Box.MaxLength = 11;
            this.miejscowosc2Box.Name = "miejscowosc2Box";
            this.miejscowosc2Box.Size = new System.Drawing.Size(100, 20);
            this.miejscowosc2Box.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(230, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Nr domu:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 131);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Nr telefonu:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(204, 73);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Nr mieszkania:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Szukaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 371);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.usunButton);
            this.Controls.Add(this.czyscButton);
            this.Controls.Add(this.AddreaderButton);
            this.Controls.Add(this.listOfreaders);
            this.Controls.Add(this.searchPeselBox);
            this.Controls.Add(this.label1);
            this.Name = "Form4";
            this.Text = "Obsługa czytelnika";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form4_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchPeselBox;
        private System.Windows.Forms.ListBox listOfreaders;
        private System.Windows.Forms.TextBox peselBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox imieBox;
        private System.Windows.Forms.TextBox nazwiskoBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox miejscowoscBox;
        private System.Windows.Forms.TextBox ulicaBox;
        private System.Windows.Forms.TextBox mieszkanieBox;
        private System.Windows.Forms.TextBox domBox;
        private System.Windows.Forms.TextBox telefonBox;
        private System.Windows.Forms.Button AddreaderButton;
        private System.Windows.Forms.Button czyscButton;
        private System.Windows.Forms.Button usunButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox pesel2Box;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox telefon2Box;
        private System.Windows.Forms.TextBox imie2Box;
        private System.Windows.Forms.TextBox dom2Box;
        private System.Windows.Forms.TextBox nazwisko2Box;
        private System.Windows.Forms.TextBox mieszkanie2Box;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox ulica2Box;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox miejscowosc2Box;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button1;
    }
}